package com.example.b09s311est.cartavirtual;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class PostresActivity extends AppCompatActivity {

    private ListView lvItems;
    private  Adaptador adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postres);
        lvItems=(ListView) findViewById(R.id.LvItems);
        adaptador=new Adaptador(this,GetArrayItems());
        lvItems.setAdapter(adaptador);
    }




    private ArrayList<Entidad> GetArrayItems(){
        ArrayList<Entidad> listItems=new ArrayList<>();

        listItems.add(new Entidad(R.drawable.dbs_16,"Tiramisu","Delicioso postre por tan solo 8000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_17,"Flan","Delicioso flan extranjero por tan solo 6000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_18,"Torta de chocolate","Rica Torta de puro chocolate Por tan solo 8000 pesos "));
        listItems.add(new Entidad(R.drawable.dbs_19,"Torta de oreo","Amante del oreo? este es tu postre, perfecto para disfrutar en compañia Por tan solo 10000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_20,"Torta de 3 leches","Delicioso postre de 3 leches Por tan solo 10000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_21,"Copa de helado","Gigantesca copa de helado, la cual trae brownie y 2 bolas de helado, Por tan solo 7500 pesos"));
        return listItems;
    }
}
