package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class PlatoActivity extends Activity {
    private ListView lvItems;
    private  Adaptador adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plato);

        lvItems=(ListView) findViewById(R.id.LvItems);
        adaptador=new Adaptador(this,GetArrayItems());
        lvItems.setAdapter(adaptador);


    }

    private ArrayList<Entidad> GetArrayItems(){
        ArrayList<Entidad> listItems=new ArrayList<>();

        listItems.add(new Entidad(R.drawable.dbs_7,"Ajiaco","El original ajiaco por tan solo 14000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_8,"Albondigas","Unas deliciosas albondigas por el precio de 12000 pesos "));
        listItems.add(new Entidad(R.drawable.dbs_9,"Arroz Atollado","Un delicioso Arroz atollado por el precio de 11000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_10,"Caldo De Costilla","Un delicioso caldo con costilla por tan solo 10000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_11,"Carne Con Chimichurri", "Una deliciosa carne de res bañada en chimichurri por tan solo a 16000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_12,"Chuleta Valluna","Una deliciosa chuleta valluna por tan solo a 15000 pesos"));
        return listItems;
    }


}
