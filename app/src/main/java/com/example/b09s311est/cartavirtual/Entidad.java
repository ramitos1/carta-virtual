package com.example.b09s311est.cartavirtual;

public class Entidad {
    private int img;
    private String titulo;
    private String contenido;


    public Entidad(int img, String titulo, String contenido) {
        this.img = img;
        this.titulo = titulo;
        this.contenido = contenido;
    }


    public int getImg() {
        return img;
    }


    public String getTitulo() {
        return titulo;
    }

    public String getContenido() {
        return contenido;
    }
}
