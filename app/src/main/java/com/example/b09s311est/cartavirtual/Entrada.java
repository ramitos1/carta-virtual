package com.example.b09s311est.cartavirtual;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;



import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;


 public class Entrada extends Activity {
    private  ListView lvItems;
    private  Adaptador adaptador;


     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_entrada);

         lvItems=(ListView) findViewById(R.id.LvItems);
         adaptador=new Adaptador(this,GetArrayItems());
         lvItems.setAdapter(adaptador);

     }
     private ArrayList<Entidad> GetArrayItems(){
         ArrayList<Entidad> listItems=new ArrayList<>();

         listItems.add(new Entidad(R.drawable.dbs_1,"Arepas","Unas ricas arepas tamaño mini por tan solo 5000 pesos"));
         listItems.add(new Entidad(R.drawable.dbs_2,"Patacones","Unos ricos patacones con hogao por tan solo 10000 pesos"));
         listItems.add(new Entidad(R.drawable.dbs_3,"Picada","Una rica picada para comer en familia por tan solo 12000 pesos"));
         listItems.add(new Entidad(R.drawable.dbs_4,"Sopa De Tomate","Rica sopa de tomate por tan solo 9000 pesos"));
         listItems.add(new Entidad(R.drawable.dbs_51,"Sopa De pollo", "Rica Sopa de pollo por tan solo 1000 pesos"));
         listItems.add(new Entidad(R.drawable.dbs_6,"Sopa De brocolo", "Una rica y nutritiva sopa de brocoli por tan solo 8000 pesos"));

         return listItems;
     }



}
