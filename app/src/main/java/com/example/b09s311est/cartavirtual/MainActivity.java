package com.example.b09s311est.cartavirtual;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private static final  String whatsApp ="com.whatsapp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    void ConsultarUbicacion(View v){
        Intent intencion =new Intent(this,MapsActivity.class);
        startActivity(intencion);

    }

    void verMenu(View v){
        Intent intencion =new Intent(this,Carta.class);
        startActivity(intencion);

    }

    void reservarMesa(View v){
        String telefono = "3146345749";

        Uri uri = Uri.parse("smsto:" + telefono);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.setPackage("com.whatsapp");
        startActivity(intent);

    }


    void calificarServicio(View v){
        Intent intencion =new Intent(this,ServicioActivity.class);
        startActivity(intencion);

    }





}
