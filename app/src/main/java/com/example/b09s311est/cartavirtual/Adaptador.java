package com.example.b09s311est.cartavirtual;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends BaseAdapter {
    private Context context;
    private ArrayList<Entidad>lisItems;


    public Adaptador(Context context, ArrayList<Entidad> lisItems) {
        this.lisItems = lisItems;
        this.context = context;
    }


    @Override
    public int getCount() {
        return lisItems.size();
    }

    @Override
    public Object getItem(int position) {
        return lisItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Entidad Item=(Entidad) getItem(position);

        convertView= LayoutInflater.from(context).inflate(R.layout.item,null);
        ImageView img= (ImageView) convertView.findViewById(R.id.img);
        TextView titulo= (TextView)convertView.findViewById(R.id.titulo);
        TextView contenido=(TextView)convertView.findViewById(R.id.contenido);

        img.setImageResource(Item.getImg());
        titulo.setText(Item.getTitulo());
        contenido.setText(Item.getContenido());



        return convertView;
    }
}
