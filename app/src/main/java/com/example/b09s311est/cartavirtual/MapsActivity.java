package com.example.b09s311est.cartavirtual;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        LatLng Sitio1 = new LatLng(6.2416235, -75.5892773);
        mMap.addMarker(new MarkerOptions().position(Sitio1).title("Sede1"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Sitio1,15));

        LatLng Sitio2 = new LatLng(6.2418621, -75.5900511);
        mMap.addMarker(new MarkerOptions().position(Sitio2).title("Sede2"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Sitio2));

        LatLng Sitio3 = new LatLng(6.2407904, -75.5906332);
        mMap.addMarker(new MarkerOptions().position(Sitio3).title("Sede3"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Sitio3));

        LatLng Sitio4 = new LatLng(6.240765, -75.5895806);
        mMap.addMarker(new MarkerOptions().position(Sitio4).title("Sede4"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Sitio4));


    }
}
