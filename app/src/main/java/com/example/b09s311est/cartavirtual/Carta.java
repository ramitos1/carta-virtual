package com.example.b09s311est.cartavirtual;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Carta extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);

    }




    void VerBebidas(View v){
        Intent intencion =new Intent(this,BebidasActivity.class);
        startActivity(intencion);

    }

    void VerEntradas(View v){
        Intent intencion =new Intent(this,Entrada.class);
        startActivity(intencion);

    }

    void VerPlatosFuertes(View v){
        Intent intencion =new Intent(this,PlatoActivity.class);
        startActivity(intencion);

    }

    void VerPostres(View v){
        Intent intencion =new Intent(this,PostresActivity.class);
        startActivity(intencion);

    }
}
