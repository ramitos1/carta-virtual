package com.example.b09s311est.cartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class BebidasActivity extends Activity {
    private ListView lvItems;
    private  Adaptador adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebidas);
        lvItems=(ListView) findViewById(R.id.LvItems);
        adaptador=new Adaptador(this,GetArrayItems());
        lvItems.setAdapter(adaptador);



    }

    private ArrayList<Entidad> GetArrayItems(){
        ArrayList<Entidad> listItems=new ArrayList<>();

        listItems.add(new Entidad(R.drawable.dbs_13,"Jugos Naturales","Deliciosos jugos en agua o en leche por tan solo a 7000 pesos"));
        listItems.add(new Entidad(R.drawable.dbs_14,"Gaseosas","Precio 5000 pesos tenemos productos postobon y cocacola"));
        listItems.add(new Entidad(R.drawable.dbs_15,"Cervezas","Precio 5000 pesos tenemos distintos tipos de cerveza bien fria "));

        return listItems;
    }




}
